/**
 * @name: 拨打电话
 * @param {String} phoneNumber
 * @return {void}
 * @example callPhone('15234855555')
 */
export default function callPhone(phoneNumber) {
  window.location.href = `tel:${phoneNumber}`;
}
