/**
 * @name: 浏览器下载blob文件流
 * @param {Blob} blob
 * @param {String} filename
 * @return {void}
 * @example downloadBlob(blob, 'test.xlsx')
 */
export default function downloadBlob(blob, filename) {
  const a = document.createElement('a');
  const href = window.URL.createObjectURL(blob);
  a.href = href; // 创建下载的链接
  a.download = filename; // 下载后文件名
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
  window.URL.revokeObjectURL(href); // 释放掉blob对象
}