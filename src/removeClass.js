import trim from './trim';
import getElem from './getElem';

/**
 * @name: 移除class
 * @param {string|Element|Element[]} selector
 * @param {string} value
 * @return {void}
 * @example removeClass('body', 'class1')
 */
export default function removeClass(selector, value) {
  value = trim(value);
  if (value) {
    const classes = value.split(/\s+/g);
    getElem(selector).forEach(el => {
      let cur = (el.getAttribute('class') || '').trim();
      if (cur && (cur = ' ' + cur + ' ')) {
        classes.forEach(cls => {
          if (cur.indexOf(' ' + cls + ' ') > -1) {
            cur = cur.replace(' ' + cls + ' ', ' ');
          }
        });
        if (cur && (cur = cur.trim())) {
          el.setAttribute('class', cur);
        } else {
          el.removeAttribute('class');
        }
      }
    });
  }
}