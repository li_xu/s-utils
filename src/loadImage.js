/**
 * @name: 加载一张图片
 * @param {String} src
 * @return {Promise}
 */
export default function loadImage(src) {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = resolve;
    img.onerror = reject;
    img.src = src;
  });
}