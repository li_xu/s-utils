import trim from './trim';
import getElem from './getElem';

/**
 * @name: 添加class
 * @param {string|Element|Element[]} selector
 * @param {string} value
 * @return {void}
 * @example addClass('body','class1')
 */
export default function addClass(selector, value) {
  value = trim(value);
  if (value) {
    const classes = value.split(/\s+/g);
    getElem(selector).forEach(el => {
      let cur = ' ' + (el.getAttribute('class') || '').trim() + ' ';
      classes.forEach(cls => {
        if (cur.indexOf(' ' + cls + ' ') < 0) {
          cur += cls + ' ';
        }
      });
      el.setAttribute('class', cur.trim());
    });
  }
}