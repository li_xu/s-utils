/**
 * @name: 获取根节点到匹配节点的链数组
 * @param {Array} list
 * @param {string} childrenKey
 * @param {Function} validator
 * @param {Array} matcheds
 * @return {Array}
 */
export default function getMatcheds(list, childrenKey, validator, matcheds = []) {
  for (let i = 0, l = list.length; i < l; i++) {
    const item = list[i];
    if (validator(item, matcheds)) {
      matcheds.push(item);
      return matcheds;
    } else if (item[childrenKey] && item[childrenKey].length) {
      const result = getMatcheds(item[childrenKey], childrenKey, validator, matcheds.concat(item));
      if (result) {
        return result;
      }
    }
  }
}