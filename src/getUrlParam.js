/**
 * @name: 获取url参数
 * @param {string} name
 * @param {string?} url 默认 window.location.search
 * @return {string}
 * @example getUrlParam('a','/index?a=1&b=2') // 1
 */
export default function getUrlParam(name, url) {
  const reg = new RegExp('(\\?|&|^)' + name + '=([^&]*)(&|$)');
  const r = (url || window.location.search).match(reg);
  return r ? unescape(r[2]) : '';
}