import isNumber from './isNumber';

/**
 * @name: 添加单位
 * @param {String|Number} value ''
 * @param {String} unit px
 * @return {String} ''
 * @example addUnit(30) // 30px
 */
export default function addUnit(value = '', unit = 'px') {
  return isNumber(value) ? Number(value) + unit : value;
}
