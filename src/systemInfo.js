/**
 * @name: 设备信息
 */
const inBrowser = typeof window !== 'undefined';
const ua = inBrowser ? window.navigator.userAgent.toLowerCase() : '';
const isMobile = !!ua.match(/AppleWebKit.*Mobile.*/i);
const isWeixin = !!ua.match(/MicroMessenger/i);
const isIE = /msie|trident/.test(ua);
const isIE9 = ua.indexOf('msie 9.0') > 0;
const isEdge = ua.indexOf('edge/') > 0;
const isAndroid = ua.indexOf('android') > 0;
const isIOS = /iphone|ipad|ipod|ios/.test(ua);
const isChrome = /chrome\/\d+/.test(ua) && !isEdge;
const isIPhone = ua.indexOf('iphone') > -1; // 是否为iPhone或者QQHD浏览器
const isIPad = ua.indexOf('ipad') > -1; // 是否iPad
const isWebApp = ua.indexOf('safari') === -1; // 是否web应该程序，没有头部与底部
const hasTouch = isMobile;
const mousedown = hasTouch ? 'touchstart' : 'mousedown';
const mousemove = hasTouch ? 'touchmove' : 'mousemove';
const mouseup = hasTouch ? 'touchend' : 'mouseup';
const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;

const systemInfo = {
  inBrowser,
  ua,
  isMobile,
  isWeixin,
  isIE,
  isIE9,
  isEdge,
  isAndroid,
  isIOS,
  isChrome,
  isIPhone,
  isIPad,
  isWebApp,
  hasTouch,
  mousedown,
  mousemove,
  mouseup,
  windowWidth,
  windowHeight,
};

export default systemInfo;
