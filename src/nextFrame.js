import systemInfo from './systemInfo';

/**
 * @name: 动画执行时机
 * @param {Function} fn
 * @return {void}
 */
export default function nextFrame(fn) {
  const raf = (systemInfo.inBrowser && window.requestAnimationFrame) ? window.requestAnimationFrame.bind(window) : setTimeout;
  return raf(() => raf(fn, 5), 5);
}