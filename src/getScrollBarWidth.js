/**
 * @name: 获取滚动条宽度
 * @return {Number}
 */
export default function getScrollBarWidth() {
  const el = document.createElement('p');
  const styles = {
    width: '100px',
    height: '100px',
    overflowY: 'scroll',
  };
  for (const k of Object.keys(styles)) {
    el.style[k] = styles[k];
  }
  document.body.appendChild(el);
  const scrollBarWidth = el.offsetWidth - el.clientWidth;
  el.parentNode.removeChild(el);
  return scrollBarWidth;
}