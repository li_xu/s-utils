import getElem from './getElem';

/**
 * @name: 获取浏览器中最大z-index值
 * @param {string|Element|Element[]} selector
 * @param {number} minZindex
 * @return {number}
 */
export default function getMaxZindex(selector = '*', minZindex = 1) {
  return Math.max.apply(null, [Math.max(1, parseInt(minZindex) || 1)].concat(getElem(selector).map(el => {
    return parseInt(el.style.zIndex) || 1;
  })));
}