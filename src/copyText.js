import getElem from './getElem';

/**
 * @name: 复制文本
 * @param {string} value
 * @return {void}
 */
export default function copyText(value) {
  const input = getElem(`<input value="${value}" style="position: absolute;left:0;top:0;opacity:0;z-index:-1"/>`)[0];
  document.body.appendChild(input);
  input.select();
  document.execCommand('copy');
  document.body.removeChild(input);
}
