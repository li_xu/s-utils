# s-utils

#### 介绍

web端常用js工具方法库，便于项目开发。

## *systemInfo*

```js
// 设备环境
systemInfo = {
  inBrowser,
  ua,
  isMobile,
  isWeixin,
  isIE,
  isIE9,
  isEdge,
  isAndroid,
  isIOS,
  isChrome,
  isIPhone,
  isIPad,
  isWebApp,
  hasTouch,
  mousedown,
  mousemove,
  mouseup,
  windowWidth,
  windowHeight,
}
```

## *数据类型*

```js
/**
 * @name: 判断是否是此对象上的实例属性
 * @param {object} obj
 * @param {string} prop
 * @return {boolean}
 * @example hasOwnProp({a:1},'a') // true
 */
hasOwnProp(obj, prop)

/**
 * @name: 获取数据类型
 * @param {*} value
 * @return {string}
 * @example getType({}) // object
 */
getType(value)

/**
 * @name: 类型验证获取value，返回默认值
 * @param {*} value
 * @param {Function|Array<Function>} types
 * @param {*} defaultValue
 * @return {*}
 * @example: getTypeValue(1,Number) //1
 * @example: getTypeValue(1,Number,1) //1
 * @example: getTypeValue('0',[String,Number],0) // '0'
 */
getTypeValue(value, types, defaultValue)

/**
 * @name: 判断指定参数是否是一个纯粹的对象
 * @param {object} obj
 * @return {boolean}
 */
isPlainObject(value)

/**
 * @name: 判断是否可以转number
 * @param {String|Number} value
 * @return {boolean}
 * @example isNumber('5') // true
 */
isNumber(value)

/**
 * @name: 判断是否为function
 * @param {*} value
 * @return {boolean}
 * @example isFunction(()=>{}) // true
 */
isFunction(value)

/**
 * @name: 判断是否为object
 * @param {*} value
 * @return {boolean}
 * @example isObject({}) // true
 */
isObject(value)

/**
 * @name: 判断是否为数组
 * @param {*} value
 * @return {boolean}
 * @example isArray([]) // true
 */
isArray(value)

/**
 * @name: 判断是否为类数组
 * @param {*} o
 * @return {boolean}
 * @example isArrayLike({0:1,1:2,length:2}) // true
 */
isArrayLike(value)

/**
 * @name: 转数组
 * @param {*} list
 * @return {array}
 */
toArray(list)

/**
 * @name: 是否为promise对象
 * @param {*} value
 * @return {boolean}
 */
isPromise(value)

/**
 * @name: 判断是不是一个有效值
 * @param {*} val
 * @return {Boolean}
 * @example: isDef(1) // true
 */
isDef(val)
```

## *addUnit*

```js
/**
 * @name: 添加单位
 * @param {String|Number} value ''
 * @param {String} unit px
 * @return {String} ''
 * @example addUnit(30) // 30px
 */
addUnit(value = '', unit = 'px')
```

## *arrayDifference*

```js
/**
 * @name: 数组差集
 * @param {Array} arr1
 * @param {Array} arr2
 * @return {Array}
 * @example: arrayDifference([1,2,3],[2,3]) //[1]
 */
arrayDifference(arr1 = [], arr2 = [])
```

## *arrayIntersect*

```js
/**
 * @name: 数组交集
 * @param {Array} arr1
 * @param {Array} arr2
 * @return {Array}
 * @example: arrayIntersect([1,2,3],[2,3,4]) //[2, 3]
 */
arrayIntersect(arr1 = [], arr2 = [])
```

## *arrayUnion*

```js
/**
 * @name: 数组并集
 * @param {Array} arr1
 * @param {Array} arr2
 * @return {Array}
 * @example: arrayUnion([1,2,3],[2,3,4]) //[1, 2, 3, 4]
 */
arrayUnion(arr1 = [], arr2 = [])
```

## *base64*

```js
/**
 * @name: base64加密
 * @param {string} str
 * @return {string}
 * @example encode(123) // MTIz
 */
base64.encode(str)

/**
 * @name: base64解密
 * @param {string} str
 * @return {string}
 * @example decode('MTIz') // 123
 */
base64.decode(str = '')
```

## *bridge 与ios原生app交互*

```js
/**
 * @name: 在需要调用客户端方法的组件中（事先需要与客户端同事约定好方法名）
 * @param {string} event
 * @param {any} data
 * @param {function(*) void} callback
 * @return {void}
 */
bridge.callhandler(name, data, callback)

/**
 * @name: 当客户端需要调用 js 函数时,事先注册约定好的函数即可
 * @param {string} event
 * @param {function(*,function?):void} callback
 * @return {void}
 */
bridge.registerhandler(name, data, callback)
```

## *callPhone 打电话*

```js
/**
 * @name: 拨打电话
 * @param {String} phoneNumber
 * @return {void}
 * @example callPhone('15234855555')
 */
callPhone(phoneNumber)
```

## *cookieStorage 浏览器cookie操作方法*

```js
/**
 * @name: 设置cookie
 * @param {string} key
 * @param {string} value
 * @param {number} days
 * @param {object} options
 * @return {void}
 */
cookieStorage.setItem(name, value, days, params = {})

/**
 * @name: 获取cookie
 * @param {string} key
 * @return {string}
 */
cookieStorage.getItem(name)

/**
 * @name: 删除cookie
 * @param {string} key
 * @param {object} options
 * @return {void}
 */
cookieStorage.removeItem(name, params = {})

/**
 * @name: 清空所有cookie
 * @param {object} options
 * @return {void}
 */
cookieStorage.clear(params = {})

/**
 * @name: 计算属性，获取cookie长度
 * @return {Number}
 */
cookieStorage.length
```

## *copyText*

```js
/**
 * @name: 复制文本
 * @param {string} value
 * @return {void}
 */
copyText(value)
```

## *countDown*

```js
/**
 * @name: 倒计时控制器
 * @param {Number} seconds 秒数
 * @param {Function(Number)} callback 每减一秒回掉
 * @param {Function} complete 倒计时完后回掉
 * @param {Number} wait 间隔事件 1000 单位ms
 * @param {Boolean} immediate 初始化是否启动，默认true
 * @return {object} {start:Function(newSeconds),stop:Function} 返回一个对象，可暂停和启动
 * @example countDown(60,second=>console.log(second),()=>console.log('完毕'))
 */
countDown(seconds = 0, callback = noop, complete = noop, wait = 1000, immediate = true)
```

## *createTimer*

```js
/**
 * @name: 创建一个定时控制器
 * @param {function} fn 每次执行回调
 * @param {number} wait 间隔时间 5000
 * @param {boolean} immediate 初始化是否启动，false
 * @return {object} {start:Function,stop:Function}返回一个对象，可暂停和启动
 * @example createTimer(()=>console.log('执行'),5000)
 */
createTimer(fn, wait = 5000, immediate = false)
```

## *debounce*

```js
/**
 * @name: 函数防抖
 * @param {Function} func 回调函数
 * @param {Number} wait 等待时间
 * @param {Boolean} immediate 首次触发是否立即执行，true
 * @return {Function}
 * @example const func = debounce(()=>console.log(1),100);
 */
debounce(func, wait = 100, immediate = true)
```

## *throttle*

```js
/**
 * @name: 函数节流
 * @param {Function} func 回调函数
 * @param {Number} wait 等待时间
 * @param {{leading:Boolean,trailing:Boolean}} options
 * @return {Function}
 * @example const func = throttle(()=>console.log(1),100);
 */
throttle(func, wait = 100, options)
```

## *downloadBlob*

```js
/**
 * @name: 浏览器下载blob文件流
 * @param {Blob} blob
 * @param {String} filename
 * @return {void}
 * @example downloadBlob(blob, 'test.xlsx')
 */
downloadBlob(blob, filename)
```

## *each*

```js
/**
 * @name: 数组和对象循环
 * @param {object|array} obj
 * @param {function(item,key,obj):boolean?} callback(item,key,obj)
 * @return {void}
 * @example each([1,2,3], (item,index,list)=>{})
 */
each(obj, callback)
```

## *EventEmit 订阅消息和发布消息*

```js
const eventBus = new EventEmit();

// 订阅消息
eventBus.on('message', data => {
  console.log(data);
});

// 发布消息
eventBus.emit('message', 123);
```

## *extend*

```js
/**
 * @name: 对象合并，对象拷贝
 * @param {object} args
 * @return {object}
 * @example extend(obj,obj1,obj2...) //浅合并  
 * @example extend(true,obj,obj1,obj2...) //深度合并 
 */
extend(...args)
```

## *时间相关*

```js
/**
 * @name: 判断是否可以转为一个合法的时间对象
 * @param {*} date
 * @return {boolean}
 */
isValidDate(date)

/**
 * @name: 尝试转一个合法Date对象,兼容ios
 * @param {string|Date|number} date
 * @return {Date}
 * @example parseDate('2021-05-17') // Mon May 17 2021 08:00:00 GMT+0800 (中国标准时间)
 */
parseDate(date)

/**
 * @name: 格式化时间
 * @param {Date|Number|String} date
 * @param {String} fmt = 'YYYY-MM-DD HH:mm'
 * @return {String}
 * @example formatDate(new Date(), fmt = 'YYYY-MM-DD HH:mm')
 */
formatDate(date, fmt = 'YYYY-MM-DD HH:mm')

/**
 * @name: 格式化时间段
 * @param {Date|Number|String} startDateTime
 * @param {Date|Number|String} endDateTime
 * @param {String} separator ~
 * @param {String} startformat
 * @param {String} endformat
 * @return {String}
 * @example formatDateRange(new Date(),new Date())
 */
formatDateRange(
  startDateTime,
  endDateTime,
  separator = ' ~ ',
  startformat = 'YYYY-MM-DD HH:mm',
  endformat = 'YYYY-MM-DD HH:mm',
)

/**
 * @name: 格式化日期为今天，明天，后天，周几
 * @param {Date | String | Number} date
 * @param {Number} num 0,1,2,3
 * @return {String}
 * @example formatDay(new Date())
 */
formatDay(date, num)

/**
 * @name: 格式化时间差，
 * @param {Date|Number|String} date
 * @param {Date|Number|String} now 默认与当前时间相比
 * @param {Number} maxDays
 * @param {String} nowStr '刚刚'
 * @return {String}
 * @example formatDiffTime(date, now, maxDays = 7, nowStr = '刚刚')
 */
formatDiffTime(date, now, maxDays = 7, nowStr = '刚刚')

/**
 * @name: 格式化毫秒为对象
 * @param {Number} time
 * @param {String|Array} fmt 默认 'd,h,m,s,S'
 * @return {object} {d,d2,h,h2,m,m2,s,s2,S,S1,S2,S3}
 * @example parseMilliseconds(5*1000) // {d:0,d2:'00',h:0,h2:'00',m:0,m2:'00',s:5,s2:'05',S:0,S1:'0',S2:'00',S3:'000'}
 */
parseMilliseconds(time, fmt = 'd,h,m,s,S')

/**
 * @name: 格式化秒为对象
 * @param {Number} time
 * @param {String|Array} fmt 默认 'd,h,m,s'
 * @return {object} {d,d2,h,h2,m,m2,s,s2}
 * @example parseSeconds(5*60) // {d: 0, d2: '00', h: 0, h2: '00', m: 5, m2: '05', s: 0, s2: '00'}
 */
parseSeconds(seconds = 0, fmt = 'd,h,m,s')

/**
 * @name: 获取某年某月有多少天
 * @param {String|Number} year
 * @param {String|Number} month
 * @return {Number}
 * @example getDaysInMonth(2021,2) // 28
 */
getDaysInMonth(year = '', month = '')
```

## *formatMoney*

```js
/**
 * @name: 格式化货币
 * @param {Number} number 货币数字
 * @param {Number} places 保留的小位数,2
 * @param {String} symbol 货币符号：'￥'
 * @param {String} thousand 用啥隔开：','
 * @param {String} decimal 表示小数点符合:'.'
 * @return {String}
 * @example formatMoney(12345) // ￥12,345.00
 */
formatMoney(
  number = 0,
  places = 2,
  symbol = '￥',
  thousand = ',',
  decimal = '.'
)
```

## *toFixed*

```js
/**
 * @name: 数字保留几位小数，解决toFixed四舍五入的问题
 * @param {Number|String} number 数字
 * @param {Number} digits 小数位数
 * @return {String}
 * @example: toFixed(1234.25555,2) // '1234.25'
 */
toFixed(number, digits = 0)
```

## *formatNumber*

```js
/**
 * @name: 格式化数字
 * @param {Number} number 数字
 * @param {Number} places 保留的小位数,2
 * @param {String} thousand 用啥隔开：','
 * @param {String} decimal 表示小数点:'.'
 * @return {String}
 * @example formatMoney(12345) // 12,345
 */
formatNumber(
  number = 0,
  places = 0,
  thousand = ',',
  decimal = '.'
)
```

## *getMatcheds 获取根节点到匹配节点的链数组*

```js
/**
 * @name: 获取根节点到匹配节点的链数组
 * @param {Array} list
 * @param {string} childrenKey
 * @param {Function} validator
 * @param {Array} matcheds
 * @return {Array}
 */
getMatcheds(list, childrenKey, validator, matcheds = [])
```

## *getRandom*

```js
/**
 * @name: 获取指定位数的随机数
 * @param {number} num
 * @return {string}
 * @example getRandom(4) // 0626
 */
getRandom(num)
```

## *getUid*

```js
/**
 * @name: 生成一个uid，全局唯一标识符
 * @param {Number} len uid的长度
 * @param {Number} radix 进制数
 * @return {String}
 * @example getUid(12) // 18djrujZHyaO
 */
getUid(len = 12, radix = 0)
```

## *getUrlParam*

```js
/**
 * @name: 获取url参数
 * @param {string} name
 * @param {string?} url 默认 window.location.search
 * @return {string}
 * @example getUrlParam('a','/index?a=1&b=2') // 1
 */
getUrlParam(name, url)
```

## *loadImage*

```js
/**
 * @name: 加载一张图片
 * @param {String} src
 * @return {Promise}
 */
loadImage(src)
```

## *mergeClass*

```js
/**
 * @name: 合并多个class
 * @param {String|object|Array<String|object>} classList
 * @return {String} 返回字符串class
 * @example: mergeClass({a:false},{b:true},'c d ',['e f','g',{h:true}]) //b c d e f g h
 */
mergeClass(...classList)
```

## *mergeStyle*

```js
/**
 * @name: 合并多个style
 * @param {Array<String|object>} styleList
 * @return {String} 返回字符串style，兼容其它端
 * @example: mergeStyle('width:10rpx',{height:'20rpx',zIndex:2},'border:2rpx') //width:10rpx;height:20rpx;z-index:2;border:2rpx
 */
mergeStyle(...styleList)
```

## *mergePath*

```js
/**
 * @name: 路径拼接
 * @param {String[]} args
 * @return {String}
 * @example mergePath('pages','home') // pages/home
 */
mergePath('a/', '/b/', '/c') // a/b/c
```

## *nextFrame*

```js
/**
 * @name: 动画执行时机
 * @param {Function} fn
 * @return {void}
 */
nextFrame(fn)
```

## *noop 空方法*

```js
/**
 * @name: 用于默认Function赋值
 */
```

## *privatePhone*

```js
/**
 * @name: 隐藏手机号码中的几位数字加密显示
 * @param {String} phone
 * @return {String}
 * @example privatePhone('15234856547') // 152****6547
 */
privatePhone(phone)
```

## *toHump*

```js
/**
 * @name: -转驼峰
 * @param {String} str 字符串
 * @param {String} mark 转换符号：-
 * @return {String}
 * @example: toHump('border-radius')  // borderRadius
 */
toHump(str = '', mark = '-')
```

## *toLine*

```js
/**
 * @name: 驼峰转-
 * @param {String} str 字符串
 * @param {String} mark 转换符号：-
 * @return {String}
 * @example: toLine('borderRadius')  // border-radius
 */
toLine(str = '', mark = '-')
```

## *toValidListData*

```js
/**
 * @name: 把不规则的数据格式转换为统一的数组[{value:'1'，label:'文字'}]数据格式
 * @param {Array|object} data
 * @param {string} valueKey
 * @param {string} labelKey
 * @return {Array}
 */
toValidListData(data, valueKey = 'value', labelKey = 'label')
```

## *trim*

```js
/**
 * @name: 去掉字符串空格
 * @param {String} str
 * @param {String} pos both：左右2边空格， left：左边空格，right：右边空格，all：全部空格
 * @return {String}
 */
trim(str, pos = 'both')
```

## *utf8to16*

```js
/**
 * @name: utf8to16
 * @param {String} str
 * @return {String}
 */
utf8to16(str)
```

## *utf16to8*

```js
/**
 * @name: utf16to8
 * @param {String} str
 * @return {String}
 */
utf16to8(str)
```

## *validate 校验方法*

```js
/**
 * @name: 是否为整数
 * @param {string|number} val
 * @return {boolean}
 */
validate.isInteger(val)

/**
 * @name: 是否为正确的手机号码格式
 * @param {string} val
 * @return {boolean}
 */
validate.isPhone(val)

/**
 * @name: 是否为电子邮箱地址
 * @param {string} val
 * @return {boolean}
 */
validate.isEmail(val)

/**
 * @name: 是否带有域名
 * @param {string} val
 * @return {boolean}
 */
validate.hasHost(val)

/**
 * @name: 是否带有表情字符
 * @param {string} val
 * @return {boolean}
 */
validate.hasEmoj(val)

/**
 * @name: 是否为有效的身份证号码
 * @param {string} idCard
 * @return {boolean}
 */
validate.isIdCard(idCard)

/**
 * @name: 中文姓名判断
 * @param {string} name
 * @param {number} length
 * @return {boolean}
 */
validate.isNameZh(name, length = 2)

/**
 * @name: 港澳居民来往内地通行证，规则： H/M + 10位或6位数字，样本： H1234567890
 * @param {string} card
 * @return {boolean}
 */
validate.isHKCard(card)

/**
 * @name: 台湾居民来往大陆通行证，规则： 新版8位或18位数字， 旧版 英文字母 + 10位数字，样本： 12345678 或 1234567890B
 * @param {string} card
 * @return {boolean}
 */
validate.isTWCard(card)

/**
 * @name: 中国护照
 * @param {string} val
 * @return {boolean}
 */
validate.isPassport(val)

/**
 * @name: 规则： 军/兵/士/文/职/广/（其他中文） + "字第" + 4到8位字母或数字 + "号"，样本： 军字第2001988号, 士字第P011816X号
 * @param {string} card
 * @return {boolean}
 */
validate.isOfficerCard(val)
```

## *dom 相关*

```js
/**
 * @name: 获取或生成dom节点
 * @param {string|Element|Element[]} selector
 * @param {Element} context
 * @return {Element[]}
 * @example getElem('div')
 */
getElem(selector, context)

/**
 * @name: 添加class
 * @param {string|Element|Element[]} selector
 * @param {string} value
 * @return {void}
 * @example addClass('body','class1')
 */
addClass(selector, value)

/**
 * @name: 移除class
 * @param {string|Element|Element[]} selector
 * @param {string} value
 * @return {void}
 * @example removeClass('body', 'class1')
 */
removeClass(selector, value)

/**
 * @name: 获取浏览器中最大z-index值
 * @param {string|Element|Element[]} selector
 * @param {number} minZindex
 * @return {number}
 */
getMaxZindex(selector = '*', minZindex = 1)

/**
 * @name: 获取滚动条宽度
 * @return {Number}
 */
getScrollBarWidth()

/**
 * @name: 使用rem
 * @param {Number} styleWidth
 * @param {Number} remUnit
 * @return {void}
 */
useRem(styleWidth = 375, remUnit = 100)

/**
 * @name: 适配屏幕
 * @param {Number} size 尺寸
 * @param {Number} styleWidth 设计稿尺寸，默认375
 * @return {Number}
 */
shfitSize(size, styleWidth = 375)
```
